package com.b2cloud.technicalexercise.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.b2cloud.technicalexercise.R;
import com.b2cloud.technicalexercise.adapter.EntryAdapter;
import com.b2cloud.technicalexercise.http.EntryParser;
import com.b2cloud.technicalexercise.http.HttpRequest;
import com.b2cloud.technicalexercise.model.Entry;
import com.b2cloud.technicalexercise.recyclerViewDividerHelpers.DividerItemDecoration;
import com.b2cloud.technicalexercise.recyclerViewDividerHelpers.VerticalSpaceItemDecoration;
import com.b2cloud.technicalexercise.util.Constant;
import com.b2cloud.technicalexercise.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by hossam on 8/26/16.
 * Lists the articles fetched by the api and show the first one as a featured row.
 * stores the received JSON to sharedpreferences to access it again while offline
 */
public class MainActivity extends AppCompatActivity {
    private static final int VERTICAL_ITEM_SPACE = 24; // divider space
    private RecyclerView recyclerView;
    private TextView tvEmpty;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewsAndToolbar();

        if(Util.isNetworkConnected(MainActivity.this))
        new CallUrlTask().execute();
        else
        {
            String oldResponse = Util.getCachedData(this);
            if(oldResponse!=null)
            {
                try {
                    setRecyclerViewData(new JSONObject(oldResponse));
                } catch (JSONException e) {
                    e.printStackTrace();
                    tvEmpty.setVisibility(View.VISIBLE);
                    tvEmpty.setText(getResources().getString(R.string.text_unexpected_error));
                }
            }
            else {
                progressBar.setVisibility(View.INVISIBLE);
                tvEmpty.setVisibility(View.VISIBLE);
                tvEmpty.setText(getResources().getString(R.string.text_no_internet));

            }

        }

    }
    //initialize views and supported actionbar
    private void initViewsAndToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewFeed);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        tvEmpty = (TextView)findViewById(R.id.textViewEmpty);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, R.drawable.divider));
        recyclerView.setLayoutManager(linearLayoutManager);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


       if(id == R.id.action_refresh) {
           if(Util.isNetworkConnected(MainActivity.this))
            new CallUrlTask().execute();
           else {
               progressBar.setVisibility(View.INVISIBLE);

               Toast.makeText(MainActivity.this, getResources().getString(R.string.text_no_internet), Toast.LENGTH_LONG).show();


           }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/**
 * Calls the news API.
 * Cache the response.
 * Calls setRecyclerViewData to set the recyclerView
 *
 */

    private class CallUrlTask extends AsyncTask<String, Void, JSONObject> {


        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText(getResources().getString(R.string.text_loading));
            recyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HttpRequest req=new HttpRequest(Constant.HOST);
                JSONObject response = req.prepare().sendAndReadJSON();

                return response;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }



        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            if(response!=null){
                Util.setCacheData(MainActivity.this,response.toString());
                setRecyclerViewData(response);
            }
            else {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.text_unexpected_error), Toast.LENGTH_SHORT).show();
            }

        }
    }
    //initialiez the Entry adapter and set set the recyclerview adapter
    private void setRecyclerViewData(JSONObject response)
    {

        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);

        List<Entry>entryList = EntryParser.parseEntries(response);
        if(entryList != null) {
            EntryAdapter adapter = new EntryAdapter(MainActivity.this,entryList );
            recyclerView.setAdapter(adapter);

        }
        else {
            progressBar.setVisibility(View.INVISIBLE);
            tvEmpty.setVisibility(View.VISIBLE);
           tvEmpty.setText(getResources().getString(R.string.text_unexpected_error));
        }
    }

}

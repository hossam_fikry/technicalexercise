package com.b2cloud.technicalexercise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.b2cloud.technicalexercise.R;
import com.b2cloud.technicalexercise.util.Constant;
import com.b2cloud.technicalexercise.util.Util;
/**
 * Created by hossam on 8/26/16.
 * Receives from DetailedViewActivity the link to show the article on a webview
 */
public class WebViewActivity extends AppCompatActivity {
    private String link;
    private TextView tvEmpty;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        initViewsAndToolbar();

        Bundle extras = getIntent().getExtras();
        link = extras.getString(Constant.INTENT_LINK);
        if (link != null) {

            if(Util.isNetworkConnected(WebViewActivity.this)) {
                WebView webView = (WebView) findViewById(R.id.webView);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl(link);
                webView.setWebViewClient(new WebViewClient() {
                    //Waits for the webview to load to dimiss the loading views
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        progressBar.setVisibility(View.GONE);
                        tvEmpty.setVisibility(View.GONE);

                    }

                    //follows the url redirection
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        return true;
                    }
                });
            }
            else {
                //shows no internet on the empty view on the middle of the screen
                progressBar.setVisibility(View.INVISIBLE);
                tvEmpty.setVisibility(View.VISIBLE);
                tvEmpty.setText(getResources().getString(R.string.text_no_internet));
            }
        }
        else{
            Toast.makeText(WebViewActivity.this, getResources().getString(R.string.text_unexpected_error), Toast.LENGTH_SHORT).show();
        }

    }
    private void initViewsAndToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        tvEmpty = (TextView)findViewById(R.id.textViewEmpty);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_web_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if(id == R.id.action_share) {
            share();
            return true;
        }else if( id == android.R.id.home){
            finish();
            //NavUtils.navigateUpFromSameTask(this);
            return true;}

        return super.onOptionsItemSelected(item);
    }
    // displays a share popup to share the article link
    private void share(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getResources().getString(R.string.text_share_body)
                +link;
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent,getResources().getString(R.string.text_share_via)));
    }

}

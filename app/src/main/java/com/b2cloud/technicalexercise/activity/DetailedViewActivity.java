package com.b2cloud.technicalexercise.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.b2cloud.technicalexercise.R;
import com.b2cloud.technicalexercise.imageUtils.CustomImageView;
import com.b2cloud.technicalexercise.model.Entry;
import com.b2cloud.technicalexercise.util.Constant;

/**
 * Created by hossam on 8/26/16.
 * this activity receives from the main activity Entry object and then it shows it
 * in the detailed view
 */
public class DetailedViewActivity extends AppCompatActivity {

    private Entry entry;
    private TextView tvTitle;
    private TextView tvDate;
    private TextView tvDescription;
    private CustomImageView customImageView;
    private Button buttonShowFullArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_view);
        initViewsAndToolbar();

        Bundle extras = getIntent().getExtras();
        entry = (Entry)extras.getParcelable(Constant.INTENT_ENTRY);
        if (extras != null && entry != null) {

            setUIData();
            buttonShowFullArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(DetailedViewActivity.this, WebViewActivity.class);
                    i.putExtra(Constant.INTENT_LINK,entry.getLink());
                    startActivity(i);

                }
            });
        }
        else{

            Toast.makeText(DetailedViewActivity.this, getResources().getString(R.string.text_unexpected_error), Toast.LENGTH_SHORT).show();
        }

    }
    //initialize views and supported actionbar
    private void initViewsAndToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle  = (TextView)findViewById(R.id.textviewTitle);
        tvDate = (TextView)findViewById(R.id.textviewDate);
        tvDescription = (TextView)findViewById(R.id.textviewDescription);
        customImageView = (CustomImageView) findViewById(R.id.imageViewThumbnail);
        buttonShowFullArticle = (Button) findViewById(R.id.buttonShowFullArticle);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    //set data received by intent to views
    private void setUIData(){
        tvTitle.setText(entry.getTitle());
        tvDate.setText(entry.getPublishedDateAsString());
        tvDescription.setText(entry.getDescription());


        if(entry.getMediaGroupUrl() == null && entry.getMediaGroupUrlFeatured() != null){ // if it is featured cell
            Uri uri = Uri.parse(entry.getMediaGroupUrlFeatured());

            customImageView.setImageURI(uri);

        }else if(entry.getMediaGroupUrlFeatured() == null && entry.getMediaGroupUrl() != null ) { // if its normal cell
            Uri uri = Uri.parse(entry.getMediaGroupUrl());

            customImageView.setImageURI(uri);
        }else
            customImageView.setVisibility(View.GONE);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_web_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if(id == R.id.action_share) {
            share();
            return true;
        }else if( id == android.R.id.home){
            finish();
        //NavUtils.navigateUpFromSameTask(this);
        return true;}

        return super.onOptionsItemSelected(item);
    }

    // displays a share popup to share the article link
    private void share(){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getResources().getString(R.string.text_share_body)
                +entry.getLink();
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent,getResources().getString(R.string.text_share_via)));
    }

}

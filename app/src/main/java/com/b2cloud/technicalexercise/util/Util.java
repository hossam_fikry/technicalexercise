package com.b2cloud.technicalexercise.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;

/**
 * Created by hossam on 8/27/16.
 *
 */
public class Util {

    /**
     * checks if network is connected
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo() != null;
    }

    /**
     * Saves data to shared preferences
     * @param context
     * @param data
     */
    public static void setCacheData(Context context, String data)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constant.CACHE_LIST,data);
        editor.apply();
    }

    /**
     * retreives data from shared preferences
     * @param context
     * @return
     */
    public static String getCachedData(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String name = preferences.getString(Constant.CACHE_LIST, "");
        if(!name.equalsIgnoreCase(""))
        {
           return name;
        }
        else return null;
    }

}

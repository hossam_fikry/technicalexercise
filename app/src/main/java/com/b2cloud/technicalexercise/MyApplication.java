package com.b2cloud.technicalexercise;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

/**
 * Created by hossam on 9/14/16.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)

                .setDownsampleEnabled(true)

                .build();
        Fresco.initialize(this, config);
    }
}
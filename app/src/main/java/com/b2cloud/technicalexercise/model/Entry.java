package com.b2cloud.technicalexercise.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hossam on 8/26/16.
 * Has Entry Model attributes and its Parcelable to pass Entry objects via Intents
 */
public class Entry implements Parcelable {
    private String title;
    private String link;
    private String author;
    private String mediaGroupUrl;
    private String mediaGroupUrlFeatured;
    private String description;
    private Date publishedDate;

    public Entry() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMediaGroupUrl() {
        return mediaGroupUrl;
    }

    public void setMediaGroupUrl(String mediaGroupUrl) {
        this.mediaGroupUrl = mediaGroupUrl;
    }

    public String getMediaGroupUrlFeatured() {
        return mediaGroupUrlFeatured;
    }

    public void setMediaGroupUrlFeatured(String mediaGroupUrlFeatured) {
        this.mediaGroupUrlFeatured = mediaGroupUrlFeatured;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public Date getPublishedDateAsDate() {
        return publishedDate;
    }

    public String getPublishedDateAsString() {

        String format = "MMM dd, yyyy hh:mm a";
        DateFormat df = new SimpleDateFormat(format);
        System.out.println(publishedDate);
        return df.format(publishedDate);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }
    public void setPublishedDateFromString(String publishedDate) {
        String format = "EEE, dd MMM yyyy hh:mm:ss z";

        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        try {
            this.publishedDate = sdf.parse (publishedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            this.publishedDate = null;
        }
    }

    protected Entry(Parcel in) {
        title = in.readString();
        link = in.readString();
        author = in.readString();
        mediaGroupUrl = in.readString();
        mediaGroupUrlFeatured = in.readString();
        description = in.readString();
        long tmpPublishedDate = in.readLong();
        publishedDate = tmpPublishedDate != -1 ? new Date(tmpPublishedDate) : null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(author);
        dest.writeString(mediaGroupUrl);
        dest.writeString(mediaGroupUrlFeatured);
        dest.writeString(description);
        dest.writeLong(publishedDate != null ? publishedDate.getTime() : -1L);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Entry> CREATOR = new Parcelable.Creator<Entry>() {
        @Override
        public Entry createFromParcel(Parcel in) {
            return new Entry(in);
        }

        @Override
        public Entry[] newArray(int size) {
            return new Entry[size];
        }
    };
}
package com.b2cloud.technicalexercise.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2cloud.technicalexercise.activity.DetailedViewActivity;
import com.b2cloud.technicalexercise.R;
import com.b2cloud.technicalexercise.imageUtils.SquaredImageView;
import com.b2cloud.technicalexercise.model.Entry;
import com.b2cloud.technicalexercise.util.Constant;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

/**
 * Created by hossam on 8/26/16.
 * adapts the data to be shown on the recyclerview in the MainActivity
 */
public class EntryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private List<Entry> entries;
    public static final int ITEM_TYPE_NORMAL = 0;
    public static final int ITEM_TYPE_FEATURED = 1;
    private int lastPosition = -1;

    public EntryAdapter(Context context, List<Entry> entries) {
        this.context = context;
        this.entries = entries;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_NORMAL) {
            View normalView = LayoutInflater.from(context).inflate(R.layout.row_feed,parent, false);
            return new RowHomeViewHolder(normalView); // view holder for normal items
        } else if (viewType == ITEM_TYPE_FEATURED) {
            View headerRow = LayoutInflater.from(context).inflate(R.layout.row_feed_featured,parent, false);
            return new RowFeaturedViewHolder(headerRow); // view holder for featured items
        }
        else
            return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder,  int p) {

        final int position = viewHolder.getAdapterPosition();
        final Entry entry = entries.get(position);
        if(viewHolder.getItemViewType() == ITEM_TYPE_NORMAL) {
            final RowHomeViewHolder holder = (RowHomeViewHolder) viewHolder;

            holder.bindData(entry);
            setAnimation(holder.cardView, position);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, DetailedViewActivity.class);
                    i.putExtra(Constant.INTENT_ENTRY, entry);
                    context.startActivity(i);
                }
            });

        }
        else {
            final RowFeaturedViewHolder holder = (RowFeaturedViewHolder) viewHolder;
            holder.bindDate(entry);
            setAnimation(holder.cardView, position);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, DetailedViewActivity.class);
                    i.putExtra(Constant.INTENT_ENTRY,entry);
                    context.startActivity(i);


                }
            });


        }
    }

    @Override
    public int getItemViewType(int position) {
        //based on the design this where the featured row
        if(position != 0 )
            return ITEM_TYPE_NORMAL;
        else
            return ITEM_TYPE_FEATURED;
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    /**
     * represents the normal row.
     */
    public static class RowHomeViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDate;
        SquaredImageView ivThumbnail;
        CardView cardView;

        public RowHomeViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.textviewTitle);
            tvDate = (TextView) view.findViewById(R.id.textviewDate);
            ivThumbnail = (SquaredImageView)view.findViewById(R.id.imageViewThumbnail);
            cardView = (CardView)view.findViewById(R.id.card_view);


        }
        public void bindData(Entry entry)
        {
            tvTitle.setText(entry.getTitle());
            tvDate.setText(entry.getPublishedDateAsString());
            if (entry.getMediaGroupUrl() == null)
                ivThumbnail.setVisibility(View.GONE);
            else {
                //Uri uri = Uri.parse(entry.getMediaGroupUrl());
                int width = 500, height = 500;
                ImageRequest imageRequest =
                        ImageRequestBuilder.newBuilderWithSource(Uri.parse(entry.getMediaGroupUrl()))
                                .setResizeOptions(new ResizeOptions(width, height))
                                .setProgressiveRenderingEnabled(true)
                                .build();
                DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(imageRequest)
                        .setOldController(ivThumbnail.getController())
                        .setAutoPlayAnimations(true)
                        .build();
                ivThumbnail.setController(draweeController);
               // ivThumbnail.setImageURI(uri);
            }
        }
    }

    /**
     * represents the featured row.
     */
    public static class RowFeaturedViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDate;
        ImageView imageViewThumbnail;
        CardView cardView;

        public RowFeaturedViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.textviewTitle);
            tvDate = (TextView) view.findViewById(R.id.textviewDate);
            imageViewThumbnail = (ImageView)view.findViewById(R.id.imageViewThumbnail);
            cardView = (CardView)view.findViewById(R.id.card_view);


        }
        public void bindDate(Entry entry)
        {
            tvTitle.setText(entry.getTitle());
            tvDate.setText(entry.getPublishedDateAsString());
            if (entry.getMediaGroupUrlFeatured() == null)
                imageViewThumbnail.setVisibility(View.GONE);
            else {
                Uri uri = Uri.parse(entry.getMediaGroupUrlFeatured());

                imageViewThumbnail.setImageURI(uri);


            }
        }
    }
}

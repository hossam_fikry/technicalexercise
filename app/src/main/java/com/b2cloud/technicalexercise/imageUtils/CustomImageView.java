package com.b2cloud.technicalexercise.imageUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by hossam on 8/25/16.
 * sets the ImageView's height to be = 0.7 width to avoid setting dimen
 * avoids setting dimens.
 */
public class CustomImageView extends SimpleDraweeView {

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(),(int) (getMeasuredWidth() * 0.7));
    }
}
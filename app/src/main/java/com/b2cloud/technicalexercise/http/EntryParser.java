package com.b2cloud.technicalexercise.http;

import android.support.annotation.Nullable;

import com.b2cloud.technicalexercise.model.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hossam on 8/26/16.
 */
public class EntryParser {

    private static String RESPONSE_DATA = "responseData";
    private static String FEED = "feed";
    private static String ENTRIES = "entries";
    private static String CONTENT = "content";
    private static String MEDIA_GROUPS= "mediaGroups";
    private static String CONTENTS = "contents";
    private static String TITLE = "title";
    private static String URL = "url";
    private static String LINK = "link";
    private static String PUBLISHED_DATE= "publishedDate";
    private static String CONTENT_DESCRIPTION = "contentSnippet";

    /**
     * parses the response and returns List of Entry
     */

@Nullable
public static List<Entry> parseEntries(JSONObject jo){

    System.out.println(jo);
    List<Entry> entryList = new ArrayList<>();
    Entry entry;
    try {
        JSONObject responseData = jo.getJSONObject(RESPONSE_DATA);
        JSONObject feed = responseData.getJSONObject(FEED);
        JSONArray entries = feed.getJSONArray(ENTRIES);

        for( int i =0; i<entries.length(); i++) {
            entry = new Entry();
            JSONObject jsonEntry = entries.getJSONObject(i);
            entry.setTitle(jsonEntry.getString(TITLE));
            entry.setLink(jsonEntry.getString(LINK));
            entry.setDescription(jsonEntry.getString(CONTENT_DESCRIPTION));
            entry.setPublishedDateFromString(jsonEntry.getString(PUBLISHED_DATE));

            JSONArray mediaGroups;
            JSONArray contents;
            //checks if it has media
            if(jsonEntry.has(MEDIA_GROUPS)){

                mediaGroups = jsonEntry.getJSONArray(MEDIA_GROUPS);
                contents = mediaGroups.getJSONObject(0).getJSONArray(CONTENTS);
                if(i != 0) //currently this is the featured tow
                    //this is the best image with aspect ration that fits the design
                    entry.setMediaGroupUrl(contents.getJSONObject(4).getString(URL));
                else // normal row
                    //this is the best image with aspect ration that fits the design
                    entry.setMediaGroupUrlFeatured(contents.getJSONObject(0).getString(URL));
            }
            entryList.add(entry);


        }

    } catch (JSONException e) {
        e.printStackTrace();
        return null;
    }
return entryList;
}
}

